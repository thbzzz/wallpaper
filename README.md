# Wallpaper

## Usage

Windows only

```
python3 wallpaper.py
```

Edit code to custom wallpaper generation (color, pattern, blend mode...).

By default, the script will populate you `WP_DIR` with wallpapers, asking you to keep them or not. Comment the ```populate()``` line and uncomment the other to simply apply one of your already existing wallpapers.