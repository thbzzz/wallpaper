import ctypes
from os import listdir, remove
from os.path import join
from random import choice, randint

from requests import get

WP_DIR = "C:\\Users\\path\\to\\wallpapers"
PATTERNS = ["45degreee_fabric", "60degree_gray", "absurdidad", "always_grey", "arab_tile", "arches", "argyle", "asfalt", "assault", "az_subtle", "back_pattern", "batthern", "bedge_grunge", "beige_paper", "bgnoise_lg", "billie_holiday", "black-Linen", "black_denim", "black_linen_v2", "black_mamba", "black_paper", "black_scales", "black_thread", "black_twill", "blackmamba", "blizzard", "blu_stripes", "bo_play_pattern", "bright_squares", "brillant", "broken_noise", "brushed_alu", "brushed_alu_dark", "burried", "candyhole", "carbon_fibre", "carbon_fibre_big", "carbon_fibre_v2", "cardboard", "cardboard_flat", "cartographer", "checkered_pattern", "chruch", "circles", "classy_fabric", "clean_textile", "climpek", "concrete_wall", "concrete_wall_2", "concrete_wall_3", "connect", "cork_1", "corrugation", "cream_dust", "crisp_paper_ruffles", "crissXcross", "cross_scratches", "crossed_stripes", "crosses", "cubes", "cutcube", "daimond_eyes", "dark_Tire", "dark_brick_wall", "dark_circles", "dark_dotted", "dark_geometric", "dark_leather", "dark_matter", "dark_mosaic", "dark_stripes", "dark_wall", "dark_wood", "darkdenim3", "darth_stripe", "denim", "diagmonds", "diagonal-noise", "diagonal_striped_brick", "diagonal_waves", "diamond_upholstery", "diamonds", "dirty_old_shirt", "double_lined", "dust", "dvsup", "egg_shell", "elastoplast", "elegant_grid", "embossed_paper", "escheresque", "exclusive_paper", "extra_clean_paper", "fabric_1", "fabric_plaid", "fake_brick", "fake_luxury", "fancy_deboss", "farmer", "felt", "first_aid_kit", "flowertrail", "foggy_birds", "foil", "frenchstucco", "furley_bg", "gold_scale", "gplaypattern", "gradient_squares", "graphy", "gray_sand", "green-fibers", "green_dust_scratch", "green_gobbler", "grey", "grey_sandbag", "greyfloral", "grid", "grid_noise", "gridme", "grilled", "groovepaper", "grunge_wall", "gun_metal", "handmadepaper", "hexabump", "hexellence", "hixs_pattern_evolution", "husk", "ice_age", "inflicted", "irongrip", "knitted-netting", "kuji", "large_leather", "leather_1", "lghtmesh", "light_alu", "light_checkered_tiles", "light_grey_floral_motif", "light_honeycomb", "light_noise_diagonal", "light_toast", "light_wool", "lightpaperfibers", "lil_fiber", "lined_paper", "linen", "little_pluses", "little_triangles", "littleknobs", "low_contrast_linen", "lyonnette", "merely_cubed", "micro_carbon", "mirrored_squares", "nami", "nasty_fabric", "natural_paper", "navy_blue", "nistri", "noise_lines", "noise_pattern_with_crosslines", "noisy", "noisy_grid", "noisy_net", "norwegian_rose", "office", "old_mathematics", "old_wall", "otis_redding", "outlets", "padded", "paper", "paper_1", "paper_2", "paper_3", "paven", "perforated_white_leather", "pineapplecut", "pinstripe", "pinstriped_suit", "plaid", "polaroid", "polonez_car", "polyester_lite", "pool_table", "project_papper", "psychedelic_pattern", "purty_wood", "px_by_Gre3g", "pyramid", "quilt", "random_grey_variations", "ravenna", "real_cf", "rebel", "redox_01", "redox_02", "reticular_tissue", "retina_dust", "retina_wood", "retro_intro", "ricepaper", "ricepaper2", "rip_jobs", "robots", "rockywall", "rough_diagonal", "roughcloth", "rubber_grip", "scribble_light", "shattered", "shinecaro", "shinedotted", "shl", "silver_scales", "skelatal_weave", "skewed_print", "skin_side_up", "small-crackle-bright", "small_tiles", "smooth_wall", "snow", "soft_circle_scales", "soft_kill", "soft_pad", "soft_wallpaper", "solid", "square_bg", "squares", "stacked_circles", "starring", "stitched_wool", "strange_bullseyes", "straws", "stressed_linen", "striped_lens", "struckaxiom", "stucco", "subtle_carbon", "subtle_dots", "subtle_freckles", "subtle_orange_emboss", "subtle_stripes", "subtle_surface", "subtle_zebra_3d", "subtlenet2", "swirl", "tactile_noise", "tapestry_pattern", "tasky_pattern", "tex2res1", "tex2res2", "tex2res3", "tex2res4", "tex2res5", "textured_stripes", "texturetastic_gray", "tileable_wood_texture", "tiny_grid", "triangles", "triangles_pattern", "txture", "type", "use_your_illusion", "vaio_hard_edge", "vertical_cloth", "vichy", "vintage_speckles", "wall4", "washi", "wavecut", "weave", "white_bed_sheet", "white_brick_wall", "white_carbon", "white_carbonfiber", "white_leather", "white_paperboard", "white_plaster", "white_sand", "white_texture", "white_tiles", "white_wall", "white_wave", "whitediamond", "whitey", "wide_rectangles", "wild_oliva", "wood_1", "wood_pattern", "worn_dots", "woven", "xv", "zigzag"]
BLEND_MODES = {40: "Default", 3: "Atop", 4: "Blend", 5: "Bumpmap", 8: "Color Burn", 9: "Color Dodge", 10: "Colorize", 20: "Darken", 22: "Dst", 23: "Dstin", 24: "Dstout", 25: "Dstover", 26: "Difference", 27: "Displace", 28: "Dissolve", 29: "Exclusion", 30: "Hardlight", 31: "Hue", 33: "Lighten", 35: "Luminize", 37: "Modulate", 38: "Multiply", 41: "Overlay", 42: "Plus", 44: "Saturate", 45: "Screen", 46: "Softlight", 47: "Srcatop", 51: "Srcover", 53: "Threshold", 54: "Xor"}


class Wallpaper:
    def __init__(self, color: str="#54828E", pattern: str="cubes", blend: int=40, intensity: int=10, noise: int=0, invert: int=0, bigger: int=0):
        self.color = color.lstrip("#").upper()
        self.pattern = pattern
        self.blend = blend
        self.intensity = intensity
        self.noise = noise
        self.invert = invert
        self.bigger = bigger
    
    def apply(self, pick_one: bool=False):
        if pick_one:
            return ctypes.windll.user32.SystemParametersInfoW(20, 0, join(WP_DIR, choice(listdir(WP_DIR))), 0)
        if not self.exists():
            return False
        return ctypes.windll.user32.SystemParametersInfoW(20, 0, join(WP_DIR, str(self)), 0)
    
    def save(self, data: bytes):
        return open(join(WP_DIR, str(self)), "wb").write(data)
    
    def download(self):
        if self.exists():
            return None
        
        url = "https://bg.siteorigin.com/api/image?color=%23{}&pattern={}&blend={}&intensity={}&noise={}&invert={}&2x={}".format(
            self.color,
            self.pattern,
            self.blend,
            self.intensity,
            self.noise,
            self.invert,
            self.bigger
        )
        response = get(url)
        
        if response.status_code == 200:
            self.save(response.content)
            print(str(self))
            return True
        return response.status_code

    def delete(self):
        remove(join(WP_DIR, str(self)))

    def exists(self):
        return str(self) in listdir(WP_DIR)
    
    def __str__(self):
        return f"{self.color}-{self.pattern}-{self.blend}-{self.intensity}-{self.noise}-{self.invert}-{self.bigger}.png"

def generate_wp():
    color = ''.join([str(hex(randint(0, 0xff)))[2:].zfill(2).upper() for i in range(3)])
    pattern = choice(PATTERNS)
    wp = Wallpaper(color, pattern)
    if randint(1,5) == 6:
        blend = choice(list(BLEND_MODES.keys()))
        wp.blend = blend
    return wp

def populate():
    while 1:
        wp = generate_wp()
        wp.pattern = "cardboard"
        wp.download()
        wp.apply()

        yn = input("keep? [y/N/q] ").lower()
        if yn != "y":
            wp.delete()
            if yn == "q":
                Wallpaper().apply(pick_one=True)
                break

if __name__ == "__main__":
    # Wallpaper().apply(pick_one=True)
    populate()
